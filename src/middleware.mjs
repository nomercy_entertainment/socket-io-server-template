
export default async (req, res, next) => {
	const ok = true;
	if(ok){
		return next();
	} else {
		return res.status(401).json({
			status: 'error',
			message: 'You do not have access to this server.',
		});
	}
};
