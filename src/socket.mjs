import { Server } from 'socket.io';

var io = null;

global.socket = [];
global.io = io;
const socketCors = {
	cors: {
		origin: '*',
		methods: 'GET,PUT,POST,DELETE,OPTIONS'.split(','),
		credentials: true,
		exposedHeaders: ['Content-Length', 'Range'],
		acceptRanges: 'byes',
	},
}

export const socket = {
	connect: function (server) {
		io = new Server(server, socketCors);
		io.setMaxListeners(300);

		io.on('connection', (socket) => {
			console.log('socket connected', socket.id);

			socket.on('message', (data) => {
				io.emit('message', data);
			});

			socket.on('disconnect', (socket) => {
				console.log('socket disconnected', socket.id);
				
			});
		});
	},

	listen: function (event, values) {
		if (io) {
			io.listen(event, values);
		}
	},

	emit: function (event, values) {
		if (io) {
			io.emit(event, values);
		}
	},

	use: function (event, values) {
		if (io) {
			io.sockets.use(event, values);
		}
	},

	on: function (event, values) {
		if (io) {
			io.on(event, values);
		}
	},
};

export default socket;