import { allowedOrigins } from './networking.mjs';
import cors from 'cors';
import express from 'express';
import { fileURLToPath } from 'url';
import http from 'http';
import middleware from './middleware.mjs';
import path from 'path';
import {socket} from './socket.mjs';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const app = express();


app.use(cors({
	origin: allowedOrigins
}));

app.use((req, res, next) => {
	res.set('X-Powered-By', 'NoMercy MediaServer');
	
	if(allowedOrigins.some(o => o == req.headers.origin)){
		res.set("Access-Control-Allow-Origin", req.headers.origin);
	}
	next();
});
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.get('/api', middleware, async (req, res) => {
	return res.json({
		message: 'howdy',
	});
});

app.get('/', middleware, async (req, res) => {
	return res.sendFile(path.join(__dirname, '/index.html'));
});

let httpServer = http.createServer(app);
		httpServer
			.listen(3005, '0.0.0.0', () => {
				console.log(`server running`);
			})
			.on('error', (err) => {
				console.error({
					message: 'Sorry Something went wrong starting the secure server',
					
				});
				process.exit(1);
			});

		socket.connect(httpServer);